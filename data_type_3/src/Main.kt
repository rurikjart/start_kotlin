fun main(args: Array<String>){

    var a1: String = "123"

    var a6: Byte = 12; //8
    var a0: Short = 11; //16
    var a2: Int = 123; //32
    var a5: Float = 1.23F; //32
    var a4: Double = 1.23e2; //64
    var a3: Long = 123; //64

    var b1: Long = a2.toLong();

    var c1: Char = 'a';
    var b3: Boolean = true;
    var b4: Boolean = false;
}