fun main(args: Array<String>){
   val userJava = UserJava("test1@test.org")
    println("Java example: ${userJava.email}")

    val userKotlin = UserKotlin("test2@test.org");
    println("Kotlin example: ${userKotlin.email}")

}