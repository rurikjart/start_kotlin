fun main(args: Array<String>){

    var index: Int = 0
    var nums = 1..10

    //цикл с предусловием
    while (index < 10) {
        print("YO :)")
        index++
    }

    do {
        print("YO :)")
        index++
    } while (index < 10)

    for (value in nums){
        print("$value")
    }

    for (value in 10 downTo 1 step 3){
        print("$value")
    }

}